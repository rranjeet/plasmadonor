package main

import (
	"context"

	//"gitlab.com/rranjeet/plasmadonor/models"
	//"gitlab.com/rranjeet/plasmadonor/services/donor/processors"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"plasmadonor/models/golang"
	"plasmadonor/services/donor/processors"

	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
)

func main() {

	// Initialize a context
	ctx, cancel := context.WithCancel(context.Background())
	bahamut.InstallSIGINTHandler(cancel)

	// Initialize a logger
	logger, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(logger.WithOptions(zap.IncreaseLevel(zapcore.DebugLevel)))

	// Initialize the server
	server := bahamut.New(
		bahamut.OptServiceInfo("donor", "1.0", nil),
		bahamut.OptModel(map[int]elemental.ModelManager{0: models.Manager(), 1: models.Manager()}),
		bahamut.OptRestServer("127.0.0.1:8000"),
	)

	// Register the processor of pizzas for the Pizza resource.
	bahamut.RegisterProcessorOrDie(server, processors.NewDonorProcessor(), models.DonorIdentity)

	// Run the server
	server.Run(ctx)

	<-ctx.Done()
}
