package processors

import (
	"net/http"
	"sync"

	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
	"go.uber.org/zap"
	"plasmadonor/models"
)

// DonorProcessor processes requests coming on /donors endpoint
type DonorProcessor struct {
	lameDB        map[string]*models.Donor
	lameDBLock    sync.RWMutex
	lameNextIndex int
}

// NewDonorProcessor creates an instance of the Donor processor.
func NewDonorProcessor() *DonorProcessor {

	return &DonorProcessor{
		lameNextIndex: 3,
		lameDB: map[string]*models.Donor{
			"1": {ID: "1", Name: "Meat Lover"},
			"2": {ID: "2", Name: "Margarita"},
		},
	}
}

// ProcessRetrieveMany implements the retrieve-many capability as a bahamut Processor.
func (p *DonorProcessor) ProcessRetrieveMany(bctx bahamut.Context) error {

	zap.L().Info("received request", zap.String("op", string(bctx.Request().Operation)))

	p.lameDBLock.RLock()
	defer p.lameDBLock.RUnlock()

	out := models.DonorsList{}
	for _, p := range p.lameDB {
		out = append(out, p)
	}

	bctx.SetOutputData(out)

	return nil
}
