# Model
model:
  rest_name: root
  resource_name: root
  entity_name: Root
  package: root
  group: core
  description: root object.
  get:
    description: gets the object.
  root: true

# Relations
relations:
- rest_name: donor
  get:
    description: Retrieves the list of donors.
  create:
    description: Creates a new donor.
  delete:
    description: Delete an existing donor.
