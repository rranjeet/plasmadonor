# Model
model:
  rest_name: donor
  resource_name: donors
  entity_name: Donor
  package: default
  group: core
  description: Plasma Donor object.
  get:
    description: Gets the object.
  update:
    description: Updates the object.
  delete:
    description: Deletes the object.
  extends:
  - '@identifiable'

# Attributes
attributes:
  v1:
  - name: name
    description: The name of the donor.
    type: string
    exposed: true
    stored: true

  - name: mobileNo 
    description: Mobile number of the user.
    type: string
    exposed: true
    stored: true

  - name: bloodType
    description: Blood Type of the user.
    type: enum
    exposed: true
    stored: true
    allowed_choices:
    - APositive
    - ABPositive
    - ABNegative
    - ANegative
    - BPositive
    - BNegative
    - OPositive 
    - ONegative

  - name: pincode
    description: Pincode of the user.
    type: string
    exposed: true
    stored: true
